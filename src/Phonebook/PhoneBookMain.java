package Phonebook;

public class PhoneBookMain {

	public static void main(String[] args){
		String file = "phonebook.txt";
		PhoneBook p = new PhoneBook(file);
		ListPhoneBook list = new ListPhoneBook(file);
		
		p.readPhoneBook();
		list.addNamePhone();
		p.readPhoneBook();
	}
}
