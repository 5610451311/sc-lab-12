package AvgExam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class WriteAverage {
	
	String fileRead;
	String fileWrite;
	FileReader Reader;
	FileWriter Writer;
	
	WriteAverage(String fileRead,String fileWrite){
		this.fileRead = fileRead;
		this.fileWrite = fileWrite;
	}

	public void averageScore(){
		
		double sum = 0;
		int n = 0;
		
		try {
			Reader = new FileReader(fileRead);
			BufferedReader buffer = new BufferedReader(Reader);
			
			Writer = new FileWriter(fileWrite,true);
			PrintWriter out = new PrintWriter(new BufferedWriter(Writer));
			
			out.println(">>>Exam Score<<");
			out.println("Name   Average");
			out.println("____   _______");
		
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] score = line.split(", ");
				out.print(score[0]);
				for(int i=1 ; i<=score.length-1;i++){
					double point = Double.parseDouble(score[i]);
					sum = sum+point;
					n++;
				}
				out.println("\t"+sum/n);
				n=0;sum=0;
			}
			out.println("--------------------------");
			out.flush();

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+fileRead);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (Reader != null)
					Reader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
			
	}
}

	

